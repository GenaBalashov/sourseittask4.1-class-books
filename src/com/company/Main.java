package com.company;

import com.company.model.Books;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Books[] book = new Books[4];


        //generate
        for (int i = 0; i < 4; i++) {
            Books books = new Books();
            books.name = "Book " + i;
            books.author = "Author " + i;
            books.yearPublishing = 1997 + i;
            book[i] = books;
        }


        //print
        for (Books books : book) {
            System.out.println("---");
            System.out.println(books.name);
            System.out.println(books.author);
            System.out.println(books.yearPublishing);
        }


        //data processing (search old book)
        int numberOldBook = 0;
        for (int i = 1; i < 4; i++) {
            if (book[0].yearPublishing > book[i].yearPublishing) {
                numberOldBook = i;
            }
        }
        System.out.println("Автор самой старой книги: " + book[numberOldBook].author);


        //data processing (search for a book by a particular author)
        System.out.println("Введите имя автора: ");
        String searchAuthor = in.nextLine();
        for (int i = 0; i < 4; i++) {
            if (searchAuthor.equalsIgnoreCase(book[i].author)) {
                System.out.println("По имени заданного автора найдены такие книги: " + book[i].name);
            }
        }


        //data processing (books published earlier than entered year)
        System.out.println("Введите год: ");
        int toYearPublish = in.nextInt();
        System.out.println("Найденные такие книги, как: ");
        for (int i = 0; i < 4; i++) {
            if (book[i].yearPublishing < toYearPublish) {
                System.out.println("----------------------------");
                System.out.println("Книга № " + i);
                System.out.println("Название: " + book[i].name);
                System.out.println("Автор: " + book[i].author);
                System.out.println("Год выпуска: " + book[i].yearPublishing);

            }
        }
    }
}
